package main

import (
	"fmt"
	"os"
)

func LowerCase(woI []byte) (woO []byte) {
	l := len(woI)
	woO = make([]byte, l)
	for i := 0; i < l; i++ {
		c := woI[i]
		if c > ('A'-1) && c < ('Z'+1) {
			woO[i] = c - 'A' + 'a'
		} else {
			woO[i] = c
		}
	}
	return
}
func main() {
	fmt.Println("type some words then [Enter]:")

	buf := make([]byte, 999)
	counts := make(map[string]int)
	n, err := os.Stdin.Read(buf)
	if err != nil {
		os.Stderr.WriteString(err.Error())
		os.Exit(1)
	}
	bgn := 0
	for bgn < n {
		for bgn < n && buf[bgn] < (' '+1) { //skip whitespace
			bgn++
		}
		end := bgn
		for end < n && buf[end] > ' ' { //for non whitespace
			end++
		}
		if bgn < end {
			counts[string(LowerCase(buf[bgn:end]))]++
		}
		bgn = end + 1
	}
	fmt.Println("word counts:", counts)
}
